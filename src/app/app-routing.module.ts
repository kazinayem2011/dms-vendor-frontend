import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignInComponent} from './components/auth/sign-in/sign-in.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';


const routes: Routes = [
    {path: 'login', component: SignInComponent},
    {path: 'dashboard', component: DashboardComponent},
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '/login'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
