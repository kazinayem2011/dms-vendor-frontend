import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private api: HttpClient
  ) { }
  
  getAllProducts() {
    return this.api.get(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/product`);
  }
  
  getAllOrders() {
    return this.api.get(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/order`);
  }
  
  myOrder(id) {
    return this.api.get(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/order/` + id);
  }
  
  postOrder(data) {
    return this.api.post(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/order/place`, data);
  }
  
  updateOrder(data) {
    return this.api.post(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/order/update`, data);
  }
  
  uploadProduct(data){
    const headers = new HttpHeaders().set('Content-Type', 'text/csv');

    return this.api.put(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/product/upload`, data, {headers: headers})
  }
}
