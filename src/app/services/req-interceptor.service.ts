import {Injectable} from '@angular/core';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class ReqInterceptorService implements HttpInterceptor {
    constructor(
        private router: Router,
        private auth: AuthService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        try {
            var session = JSON.parse(localStorage.getItem('session'));
            var accesToken = session['id_token'];
        }catch (e) {
            accesToken = null;
        }

        let modified = req.clone();
        if(accesToken !== null){
          
            modified = req.clone({setHeaders: {'Authorization': 'Bearer ' + accesToken}});
        }

        return next.handle(modified).pipe(
            catchError((err: HttpErrorResponse) => {
                localStorage.clear();
                
                // @ts-ignore
                this.router.navigate(['/login']);
                return throwError(err);
            })
        );
    }
}
