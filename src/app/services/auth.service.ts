import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private api: HttpClient
    ) {
    }

    auth(opt) {
        return this.api.put(`https://srnax0lyc0.execute-api.us-west-2.amazonaws.com/dev/signup`, opt);
    }
}
