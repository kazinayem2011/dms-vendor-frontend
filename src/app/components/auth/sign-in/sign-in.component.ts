import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
    loginForm: any;

    constructor(
        private formBuilder: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private toastr: ToastrService,
    ) {
    }

    ngOnInit(): void {
        let session = localStorage.getItem('session');
        if (session) {
            this.router.navigate(['/dashboard'])
        }
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    onSubmit() {
        !this.loginForm.invalid &&
        this.auth.auth(this.loginForm.value).subscribe(
            (user: any) => {
                localStorage.setItem('session', JSON.stringify(user));
                this.router.navigate(['/dashboard']);
            },
            error => {
                error &&
                this.toastr.error(
                    error.statusText + '!' || 'Error!',
                    error.error
                );
            }
        );
    }

}
