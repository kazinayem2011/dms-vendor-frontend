import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    orderStatusForm: any;
    user: any;
    productAll: any;
    currentMenu = 'dashboard';
    allOrders: any;
    myOrders: any;
    file: any;
    waitingFor = '';
    productDetail: any;

    constructor(
        private router: Router,
        private api: ApiService,
        private toastr: ToastrService,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnInit(): void {
        const currentMenu = localStorage.getItem('currentMenu');
        if (currentMenu != null) {
            this.currentMenu = currentMenu;

        } else {
            localStorage.setItem('currentMenu', this.currentMenu);
        }

        let session = localStorage.getItem('session');
        if (session) {
            this.user = JSON.parse(session);
            if (this.currentMenu == 'dashboard') {
                this.getAllProducts(this.currentMenu);
            } else {
                this.myOrder(this.currentMenu);
            }
        } else {
            this.router.navigate(['/login']);
        }

        this.orderStatusForm = this.formBuilder.group({
            status: ['', Validators.required],
        });
    }

    productDetails(pd) {
        this.productDetail = pd;
    }

    myOrder(menuName) {
        console.log(this.user.user_id);
        localStorage.setItem('currentMenu', menuName);
        this.currentMenu = menuName;
        this.api.myOrder(this.user.user_id).subscribe(res => {
            console.log(res)
            this.myOrders = res['body'].items;
        });
    }

    getAllProducts(menuName) {
        localStorage.setItem('currentMenu', menuName);
        this.currentMenu = menuName;
        this.api.getAllProducts().subscribe(
            (data: any) => {
                console.log(data);
                this.productAll = data;
            },
            error => {
                console.log(error);
            }
        );
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    createOrder(product, qty) {
        if (qty <= 0) {
            alert('Zero quantity is not allowed.');
            return false;
        }

        let payload = {
            'items': [
                {
                    'quantity': qty,
                    'product_id': product.id,
                    'customer_id': this.user.user_id
                }
            ]
        };

        this.api.postOrder(payload).subscribe(res => {
                if (res['items'][0]['status'] == 'success') {
                    this.toastr.success('Success', 'Order created successfully.');
                    this.myOrder('my_order');
                } else {
                    this.toastr.error('Error', 'Failed to order.');
                }
            },
            error => {
                error &&
                this.toastr.error(
                    error.statusText + '!' || 'Error!',
                    error.error
                );
            });
    }

}
